<%@include file="/libs/foundation/global.jsp"%>

<c:set var="pagetitle" value="<%=properties.get("jcr:title")%>" />

<h1>Salesforce API Test Page</h1>
<form method="GET">
  <div class="outer">
    <span style="width:120px;display:inline-block;">Property Code:</span> <input type="text" name="propertyCode" value="37030" style="width:600px;"/><br>
    <span style="width:120px;display:inline-block;">Brand Code:</span> <input type="text" name="brandCode" value="TL" style="width:250px;"/><br>
  </div>
    <input type="submit" value="invoke"/>
</form>
<%
    com.wyn.integration.SalesforceClient client = sling.getService(com.wyn.integration.SalesforceClient.class);
    client.useSandbox(true);

	String propertyCode = request.getParameter("propertyCode");
	String brandCode = request.getParameter("brandCode");
	log.error("Hello");

    if(""!=propertyCode && null!=brandCode) {
        String data = client.getSFReportDataForPropertyAndBrandCode(propertyCode, brandCode);
	%> <br> <textarea rows="25" cols="140"><%=data%></textarea>  <%
    } else if(""!=request.getParameter("propertyCode")) {
        String data = client.getSFReportDataForPropertyCode(request.getParameter(propertyCode));
    %> <br> <textarea rows="25" cols="140"><%=data%></textarea>  <%
    }
%>
<div class="container">
    <div class="row"><p></p>
        <div class="col-sm-12">
			<cq:include path="main" resourceType="foundation/components/parsys"/>
        </div>
    </div>
</div>

