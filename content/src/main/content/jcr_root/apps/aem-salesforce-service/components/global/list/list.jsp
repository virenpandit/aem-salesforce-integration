<%@page session="false"%><%--
  Copyright 1997-2009 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Extended-List component

--%><%@ page import="com.day.cq.wcm.api.WCMMode,
                   com.day.cq.wcm.api.components.DropTarget,
                   com.day.cq.wcm.foundation.List,
                   java.util.Iterator,
				   org.apache.sling.api.resource.ResourceUtil,
				   org.apache.commons.lang3.StringUtils"%><%
%><%@include file="/libs/foundation/global.jsp"%><%

	String restUrl = ResourceUtil.getValueMap(resource).get("resturl", "");

	if(StringUtils.isNotBlank(restUrl)) {
		System.out.println("SOP:list.jsp: restUrl=" + restUrl);

		%><cq:include script="list_fromservice.jsp"/><%
    } else {
		%><cq:include script="list_core.jsp"/><%
    }

%>
