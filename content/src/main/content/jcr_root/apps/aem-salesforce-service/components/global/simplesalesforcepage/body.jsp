<%@include file="/libs/foundation/global.jsp"%>

<c:set var="pagetitle" value="<%=properties.get("jcr:title")%>" />

<h1>Salesforce</h1>
<!--
<form method="GET">
  <div class="outer">
    <span style="width:120px;display:inline-block;">Property Code:</span> <input type="text" name="propertyCode" value="37030" style="width:600px;"/><br>
    <span style="width:120px;display:inline-block;">Brand Code:</span> <input type="text" name="brandCode" value="TL" style="width:250px;"/><br>
  </div>
    <input type="submit" value="invoke"/>
</form>
-->
<div class="container">
    <div class="row"><p></p>
        <div class="col-sm-12">
			<cq:include path="main" resourceType="foundation/components/parsys"/>
        </div>
    </div>
</div>
