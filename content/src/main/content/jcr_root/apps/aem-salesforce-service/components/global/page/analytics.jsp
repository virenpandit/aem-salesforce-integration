<%@ include file="/libs/foundation/global.jsp" %>
<%@ page import="java.util.Enumeration,java.util.HashMap,java.util.Map,com.wyn.myportal.util.MyPortalAnalyticsHelper,com.wyn.myportal.ConfigService" %>

<c:set var="lang" value="<%=currentPage.getAbsoluteParent(2).getTitle()%>"/>
<c:set var="pagetitle" value="<%=properties.get("jcr:title")%>" />

<%
	ConfigService configService = sling.getService(ConfigService.class);
	String account = configService.getProperty("OMNITURE-ACCOUNT");
    Map<String, String>  headermap = MyPortalAnalyticsHelper.getHeaders(request,currentNode,slingRequest.getResourceResolver());
	pageContext.setAttribute("headermap",headermap);
%>
<!-- Omniture Code -->
<script type="text/javascript">
		var s_account="<%=account%>"
        var wyn_pagename='${pagetitle}';
		var wyn_username='${headermap['fullname']}';
		var wyn_userid='${headermap['userid']}';
		var wyn_userrole='${headermap['role']}';
		var wyn_userstatus='${headermap['status']}';
		var wyn_sitenumber='${headermap['property']}';
		var wyn_brand='${headermap['brands']}';
		var wyn_country='${headermap['country-code']}';
		var wyn_region='${headermap['region']}';
		var wyn_dosname='${headermap['NA']}';
		var wyn_language='${lang}';
		var wyn_email='${headermap['email']}';
		var wyn_partyid='${headermap['partyid']}';
		var wyn_brandpublishdate='${headermap['publishedDate']}';
		var wyn_brandauthor='${headermap['NA']}';
		var wyn_branddateviewed='${headermap['revDate']}';
		var wyn_brandcategory='${headermap['category']}';
</script>

<script type="text/javascript" src="/etc/designs/myportal/clientlibs/js/s_code_min.js"></script>
<script type="text/javascript">var s_code=s.t(); if(s_code)document.write(s_code);</script>

<!-- Omniture  -->