package com.wyn.integration;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.ArrayList;

import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;

import org.apache.sling.api.SlingHttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service(value = SalesforceClient.class)
@Component(metatype = false)
public class SalesforceClientImpl implements SalesforceClient {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());
    
    public static final String REQUEST_PROPCODE     =   "prop";
    public static final String REQUEST_BRANDCODE    =   "brand";
    @Reference
    private SalesforceOSGiConfiguration osgiConfigService;

    private boolean usingSandbox = true;

    public SalesforceClientImpl() {}

    public void useSandbox(boolean usingSandbox) {
        this.usingSandbox = usingSandbox;
    }

    public ArrayList matchResultsToAEM(String jsonData, Session session, PageManager pm, int start, int count) throws Exception {
        log.debug("Inside matchResultsToAEM(range: " + start + ":" + count + ")");
        ArrayList listOfMatchingPages = new ArrayList();
        boolean morePages = true;
        LinkedHashMap<String, LinkedHashMap> dataAsMap = (new SimplerSFData()).cleanse(jsonData);
        int foundCtr = 0;
        for(String key: dataAsMap.keySet()) {
            if(morePages) {
                LinkedHashMap<String, String> rowContentsList = dataAsMap.get(key);
                for(String colKey: rowContentsList.keySet()) {
                    if(StringUtils.equalsIgnoreCase(colKey, "brandreferencekey")) {
                        String colValue = rowContentsList.get(colKey);
                        if(StringUtils.contains(colValue, "-")) {
                            colValue = StringUtils.substringBefore(colValue, "-");
                        }
                        colValue = StringUtils.replace(colValue, ".", "-");
                        log.debug("Trying to find matching page for: " + colValue);
                        Page matchingPage = findPage(colValue, session, pm);

                        if(null!=matchingPage) {
                            if(foundCtr>=start && foundCtr<(start+count)) {
                                // Add to list
                                log.debug("Match (#" + foundCtr + ") found for key: " + colValue + ", page=" + matchingPage.getName());
                                listOfMatchingPages.add(matchingPage);
                            }
                            if(foundCtr>=(count-start)) {
                                morePages=false;
                            }
                            foundCtr++;
                        }
                    }
                }
            }
        }
        log.debug("Exiting matchResultsToAEM(totalMatches=" + listOfMatchingPages.size() + ")");
        return listOfMatchingPages;
    }

    public Page findPage(String incompletePageName, Session session, PageManager pm) {
        try {
            log.debug("Inside findPage(incPath=" + incompletePageName + ")");
            String searchPathPrefix = osgiConfigService.getProperty("JCR_PAGEMATCH_PATH_PREFIX");
            String query = searchPathPrefix + "/*[jcr:contains(., '" + incompletePageName + "*')] order by @jcr:score";
            javax.jcr.query.Query jcrQuery = session.getWorkspace().getQueryManager().createQuery(query, Query.XPATH);
            QueryResult result = jcrQuery.execute();
            // result.getNodes()
            javax.jcr.NodeIterator nodes = result.getNodes();

            while (nodes != null && nodes.hasNext()) {
                Page page = pm.getContainingPage(nodes.nextNode().getPath());
                if (page != null) {
                    log.debug("Found page for path: " + incompletePageName + 
                                ", Page.getName()=" + page.getName() + 
                                ", Page.getPageTitle()=" + page.getPageTitle() + 
                                ", page.getPath()=" + page.getPath());
                    return page;
                }
            }
        } catch (Exception e) {
            log.error("Could not get page behind search result hit", e);
        }
        log.debug("Exiting findPag: No matching pages found for name; " + incompletePageName);
        return null;
    }

    public LinkedHashMap getSFReportDataForPropertyCode(String propertyCode) throws Exception {        
        log.debug("Inside getSFReportDataForProperty(propertyCode=" + propertyCode + ")");
        SalesforceReportFilters filterMetadata = new SalesforceReportFilters();
        filterMetadata.addFilterClause(SalesforceReportFilters.PROPERTYCODE, SalesforceReportFilters.EQUALS, propertyCode, SalesforceReportFilters.AND);
        String responseData = invokeServiceWithCustomFiltering(filterMetadata, null);
        LinkedHashMap dataAsMap = (new SimplerSFData()).cleanse(responseData);
        return dataAsMap;
    }

    public LinkedHashMap getSFReportDataForPropertyAndBrandCode(String propertyCode, String brandCode) throws Exception {        
        log.debug("Inside getSFReportDataForPropertyAndBrandCode(propertyCode=" + propertyCode + ", brandCode=" + brandCode + ")");
        SalesforceReportFilters filterMetadata = new SalesforceReportFilters();
        filterMetadata.addFilterClause(SalesforceReportFilters.PROPERTYCODE, SalesforceReportFilters.EQUALS, propertyCode, SalesforceReportFilters.AND);
        filterMetadata.addFilterClause(SalesforceReportFilters.BRANDCODE, SalesforceReportFilters.EQUALS, brandCode, SalesforceReportFilters.AND);
        String responseData = invokeServiceWithCustomFiltering(filterMetadata, null);
        LinkedHashMap dataAsMap = (new SimplerSFData()).cleanse(responseData);
        return dataAsMap;
    }

    public String invokeService(String serviceUrl) throws Exception {        
        log.debug("Inside invokeService(serviceUrl=" + serviceUrl + ")");
        String username = osgiConfigService.getProperty("SALESFORCE_UN");
        String password = osgiConfigService.getProperty("SALESFORCE_PW");
        return invokeService(serviceUrl, username, password);
    }

    public String invokeService(String serviceUrl, String username, String password) throws Exception {
        log.debug("Inside invokeService(url=" + serviceUrl + ", un=" + username + ", password=" + password + ")");
        HttpResponse httpLoginResponse = login(username, password);
        String httpLoginResponseAsString = inputStreamToString(httpLoginResponse.getEntity().getContent());

        if(httpLoginResponse.getStatusLine().getStatusCode() == 200) {
            String sessionId = extractSessionID(httpLoginResponseAsString);
            return invokeService(serviceUrl, sessionId);
        } else {
            log.error("Login failed: See ResponseXml: " + httpLoginResponseAsString);
            return httpLoginResponseAsString;
        }
    }

    public String invokeService(String serviceUrl, String sessionId) throws Exception {
        log.debug("Inside invokeService(" + serviceUrl + ", " + sessionId + ")");

        Response response = Request.Get(serviceUrl)
                                        .addHeader("Authorization", "Bearer " + sessionId)
                                        .execute();
        HttpResponse httpServiceResponse = response.returnResponse();
        log.debug("Service-Response:StatusCode: " + httpServiceResponse.getStatusLine().getStatusCode());
        String responseBody = inputStreamToString(httpServiceResponse.getEntity().getContent());        
        log.debug("Exiting invokeService()");
        return responseBody;
    }

    private String invokeServiceWithCustomFiltering(SalesforceReportFilters filterMetadata, String url) throws Exception { //String serviceUrl, String username, String password) throws Exception {
        log.debug("Inside invokeServiceWithCustomFiltering()");

        String serviceUrl = url;
        if(StringUtils.isBlank(serviceUrl)) {
            serviceUrl = osgiConfigService.getProperty("SALESFORCE_REPORT_URL");
        }
        String username = osgiConfigService.getProperty("SALESFORCE_UN");
        log.debug("Using Salesforce URL: " + serviceUrl + ", salesforce-user: " + username);
        String password = osgiConfigService.getProperty("SALESFORCE_PW");

        String responseBody = null;

        HttpResponse httpLoginResponse = login(username, password);
        String httpLoginResponseAsString = inputStreamToString(httpLoginResponse.getEntity().getContent());

        if(httpLoginResponse.getStatusLine().getStatusCode() == 200) {
            String sessionId = extractSessionID(httpLoginResponseAsString);

            Response response = Request.Post(serviceUrl)
                                            .useExpectContinue()
                                            .version(HttpVersion.HTTP_1_1)
                                            .addHeader("Authorization", "Bearer " + sessionId)
                                            .addHeader("Content-type", "application/json")
                                            .addHeader("charset", "UTF-8")
                                            .bodyString(filterMetadata.toString(), ContentType.APPLICATION_JSON)
                                            .execute();
            HttpResponse httpServiceResponse = response.returnResponse();
            log.debug("Service-Response:StatusCode: " + httpServiceResponse.getStatusLine().getStatusCode());

            responseBody = inputStreamToString(httpServiceResponse.getEntity().getContent());
            //log.debug("Filtered Response: " + responseBody);
        }        
        log.debug("Exiting invokeServiceWithCustomFiltering()");
        return responseBody;
    }


	public HttpResponse login(String username, String password) throws Exception {

        log.debug("Inside login(username=" + username + ")");
        String loginUrl = null;
        if(usingSandbox)
            loginUrl = osgiConfigService.getProperty("SALESFORCE_SANDBOX_LOGIN_URL");
        else
            loginUrl = osgiConfigService.getProperty("SALESFORCE_LOGIN_URL");

        String requestBody = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><env:Envelope xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"  " +
                        "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"    xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\"> " +
                        "<env:Body>    <n1:login xmlns:n1=\"urn:partner.soap.sforce.com\">      <n1:username>" + username + "</n1:username> " +
                        " <n1:password>" + password + "</n1:password>    </n1:login></env:Body></env:Envelope>";
        log.debug("### Inside login()");
	    String responseBody = null;

        log.debug("Posting to url: " + loginUrl);
        Response response = Request.Post(loginUrl)
                                            .useExpectContinue()
                                            .version(HttpVersion.HTTP_1_1)
                                            .addHeader("Content-Type", "text/xml")
                                            .addHeader("SOAPAction", "login")
                                            .addHeader("Accept", ContentType.APPLICATION_XML.getMimeType())
                                            .bodyString(requestBody, ContentType.APPLICATION_XML)
                                            .execute();
        HttpResponse httpResponse = response.returnResponse();

        log.debug("### Exiting login(): ResponseCode: " + httpResponse.getStatusLine().getStatusCode());
        return httpResponse;
	}

    private String extractSessionID(String xml) {
        String sessionId = StringUtils.substringBetween(xml, "<sessionId>", "</sessionId>");
        log.debug("SessionId=" + sessionId);
        return sessionId;
    }

	private String inputStreamToString(InputStream in) throws IOException {
		StringWriter output = new StringWriter();
		InputStreamReader input = new InputStreamReader(in);
		char[] buffer = new char[1024 * 4];
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
		}
		return output.toString();
	}
    
    public String getSalesforceDataAsJson(String url, SlingHttpServletRequest request) throws Exception {
        log.debug("Inside getSalesforceDataAsJson(url=" + url + ")");
        String responseData = "";
        String propertyCode = request.getHeader(REQUEST_PROPCODE);
        if(StringUtils.isBlank(propertyCode)) {
            propertyCode = request.getParameter(REQUEST_PROPCODE);
        }
        String brandCode = request.getHeader(REQUEST_BRANDCODE);
        if(StringUtils.isBlank(brandCode)) {
            brandCode = request.getParameter(REQUEST_BRANDCODE);
        }

        if(StringUtils.isNotBlank(propertyCode) || StringUtils.isNotBlank(brandCode)) {
            SalesforceReportFilters filterMetadata = new SalesforceReportFilters();
            if(StringUtils.isNotBlank(propertyCode)) {
                log.debug("Salesforce-filters: Filtering results for propertyCode=" + propertyCode);
                filterMetadata.addFilterClause(SalesforceReportFilters.PROPERTYCODE, SalesforceReportFilters.EQUALS, propertyCode, SalesforceReportFilters.AND);
            }
            if(StringUtils.isNotBlank(brandCode)) {
                log.debug("Salesforce-filters: Filtering results for brandCode=" + brandCode);
                filterMetadata.addFilterClause(SalesforceReportFilters.BRANDCODE, SalesforceReportFilters.EQUALS, brandCode, SalesforceReportFilters.AND);
            }
            responseData = invokeServiceWithCustomFiltering(filterMetadata, url);
        } else {
            log.debug("Salesforce-filters: Invoking Salesforce API WITH NO FILTERS. All data will be returned!");
            responseData = invokeService(url);
        }
        return responseData;
    }
}
