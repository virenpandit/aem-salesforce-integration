package com.wyn.integration;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.SlingServletException;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* Implementation of SalesforceOSGiConfiguration provides OsgiConfig parameters.
*/
@Service(value={
        SalesforceOSGiConfiguration.class,
        javax.servlet.Servlet.class
 })

@Component(immediate = true, metatype = true)
@Properties({
        @Property(name="SALESFORCE_REPORT_URL", label="Salesforce Report Url", value="http://cs8.salesforce.com/services/data/v29.0/analytics/reports/00OL0000000evt1?includeDetails=true"),
        @Property(name="SALESFORCE_LOGIN_URL", label="Salesforce Login Url", value="https://login.salesforce.com/services/Soap/u/34.0"),
        @Property(name="SALESFORCE_SANDBOX_LOGIN_URL", label="Salesforce (Sandbox) Login Url", value="https://test.salesforce.com/services/Soap/u/34.0"),
        @Property(name="SALESFORCE_UN", label="Salesforce (Sandbox) Login Url", value="viren.pandit@wyn.com.uatstaging"),
        @Property(name="SALESFORCE_PW", label="Salesforce (Sandbox) Login Url", value="viren123STtOEiTbrFMPWGuNjOW3Aezb"),
        @Property(name="JCR_PAGEMATCH_PATH_PREFIX", label="Base AEM Page to use while trying to match against incoming data", value="/jcr:root/content/myportal/en_us/brandstandardsdata/allbrands/"),
        @Property(name = Constants.SERVICE_DESCRIPTION, value = "AEM Salesforce Configuration"),
        @Property(name = Constants.SERVICE_VENDOR, value = "Wyndham"),
        @Property(name = "process.label", value = "AEM Salesforce - Configuration Service"),
		@Property(name="sling.servlet.paths", value = "/bin/config"),
        @Property(name="sling.servlet.selectors",value="salesforce"),
        @Property(name="sling.servlet.extensions",value="json")
})

public class SalesforceOSGiConfiguration extends SlingAllMethodsServlet {

	private Dictionary<String, String> properties;
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@SuppressWarnings("unchecked")
	    protected void activate(ComponentContext context) {
	        properties = context.getProperties();
	        //Check the properties are reading from OsgiConfig
	        /*for (Enumeration<String> e = properties.keys(); e.hasMoreElements();) {
	        	       log.info("Key value from OSGI Config : " + (String) e.nextElement());
	        }*/
	    }

	    protected void deactivate(ComponentContext context) {
	        properties = null;
	    }

	    public String getProperty(String key) {
	    	String config_key = null;
	    	if(null != properties){
	    		config_key = properties.get(key);
	    		log.info("Osgi config value of " + key + " is: "+ config_key);
	    	}
	        return config_key;     
	    } 

	    public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws SlingServletException, IOException {
			PrintWriter out = response.getWriter();
			String property = request.getParameter("param");
			String value = "";
			if(StringUtils.isNotBlank(property)){
				value = getProperty(property);
			}
			out.write(value);
		}
}
