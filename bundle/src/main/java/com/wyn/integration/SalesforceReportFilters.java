package com.wyn.integration;

import org.apache.commons.lang3.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
    Sample Final Output -
    {  "reportMetadata": { 
      "reportBooleanFilter": "1 AND 2",
      "reportFilters": [  
      { "value": "TL", "operator": "notEqual", "column": "QA_Item__c.Brand_Code__c" }, 
      { "value": "37030", "operator": "equals", "column": "Quality_Assurance__c.Property__c.Name" } 
    ]  } } 
*/

public class SalesforceReportFilters {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

    public static final String BRANDCODE = "QA_Item__c.Brand_Code__c";
    public static final String PROPERTYCODE = "Quality_Assurance__c.Property__c.Name";

    public static final String EQUALS = "equals";
    public static final String NOTEQUAL = "notEqual";
    public static final String CONTAINS = "contains";
    public static final String DOESNOTCONTAIN = "notContain";
    public static final String BEGINSWITH = "startsWith";
    public static final String LESSTHAN = "lessThan";
    public static final String LESSTHANOREQUALTO = "lessOrEqual";
    public static final String GREATERTHAN = "greaterThan";
    public static final String GREATERTHANOREQUALTO = "greaterOrEqual";
    
    public static final String AND = "AND";
    public static final String OR = "or";

    private int totalFilters = 0;
    public String reportFilters;
    public String reportBooleanFilter;


    public void addFilterClause(String field, String operator, String value, String joinCondition) {
        log_debug("Inside addFilterClause(field=" + field + ", operator=" + operator + ", value=" + value + ")");
        if(StringUtils.isNotBlank(field) && StringUtils.isNotBlank(operator) && StringUtils.isNotBlank(value)) {
            String filterClause = " { " + "\"value\": \"" + value + "\", \"operator\": \"" + operator + "\", \"column\": \"" + field + "\"" + " } ";
            log_debug("Filter.toString(): Returning " + filterClause);
            
            if(StringUtils.isBlank(reportFilters)) {
                reportFilters = filterClause;
                totalFilters++;
            } else {
                reportFilters = reportFilters + ", " + filterClause;
                totalFilters++;
            }

            if(StringUtils.isBlank(reportBooleanFilter)) {
                reportBooleanFilter = "( " + totalFilters + " )";
            } else {
                if(StringUtils.isBlank(joinCondition)) {
                    joinCondition = " AND ";
                }
                reportBooleanFilter = reportBooleanFilter + " " + joinCondition + " ( " + totalFilters + " ) ";
            }
            
            log_debug("Exiting addFilterClause(): New Filter clause: " + this.reportFilters);
        } else {
            log_debug("Filter is empty! Will not apply empty filter");
        }
    }

    public String toString() {
        log_debug("Inside toString()");
        String returnString = "";
        if(totalFilters>0) {
            if(totalFilters>1) {
                returnString = "{  \"reportMetadata\": {" + "\"reportBooleanFilter\": \"" + reportBooleanFilter + "\" " + ", " + " \"reportFilters\": [ " + reportFilters + " ] " + " } } ";
            } else {
                returnString = "{  \"reportMetadata\": {" + " \"reportFilters\": [ " + reportFilters + " ] " + " } } ";
            }
        } else {
            returnString = "{  \"reportMetadata\": {  } } ";
        }
        
        log_debug("Exiting toString(): returning: " + returnString);
        return returnString;
    }

    private void log_debug(Object msg) {
        System.out.println("DEBUG: " + msg);
        log.debug("" + msg);
    }
}
