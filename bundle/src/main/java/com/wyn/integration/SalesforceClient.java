package com.wyn.integration;

import java.util.LinkedHashMap;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import javax.jcr.Session;
import javax.jcr.query.QueryResult;
import org.apache.sling.api.SlingHttpServletRequest;

public interface SalesforceClient {

    public void useSandbox(boolean usingSandbox);
    
    public String getSalesforceDataAsJson(String url, SlingHttpServletRequest request) throws Exception;
    public ArrayList matchResultsToAEM(String jsonData, Session session, PageManager pm, int start, int count) throws Exception;
    public Page findPage(String incompletePageName, Session session, PageManager pm);

    public LinkedHashMap getSFReportDataForPropertyCode(String propertyCode) throws Exception;
    public LinkedHashMap getSFReportDataForPropertyAndBrandCode(String propertyCode, String brandCode) throws Exception;
}
