package com.wyn.integration;

import org.apache.commons.lang3.StringUtils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SimplerSFData {

	protected final Logger log = LoggerFactory.getLogger(SimplerSFData.class);

    public String toJSONString(LinkedHashMap dataAsMap) throws Exception {
        JSONObject data = reportDataToJson(dataAsMap);
        return data.toString();
    }

    public String translateToAEMPages(String jsonString) throws Exception {
        LinkedHashMap structuredData = cleanse(jsonString);
        JSONObject data = reportDataToJson(structuredData);
        return data.toString();
    }

    public LinkedHashMap<String, LinkedHashMap> cleanse(String jsonString) throws Exception {
        log.debug("Inside cleanse()...");
        log.debug("Replacing special characters in response-data...");
        if(StringUtils.contains(jsonString, "T!T")) {
            jsonString = StringUtils.replace(jsonString, "T!T", "TnT");
        }
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(jsonString);

		JSONObject rootObject = (JSONObject) obj;

        ArrayList<String> reportHeaders = getHeaderInfo(rootObject);
        HashMap<String, String> decorators = getColumnDecorator();

        LinkedHashMap<String, LinkedHashMap> allRowsList = new LinkedHashMap<String, LinkedHashMap>();
        JSONArray allDataRows = (JSONArray)((JSONObject) ((JSONObject) rootObject.get("factMap")).get("TnT")).get("rows");
        for(int i=0; i<allDataRows.size(); i++) {
            JSONObject dataCells = (JSONObject) allDataRows.get(i);
            JSONArray eachRow = (JSONArray)dataCells.get("dataCells");
            LinkedHashMap<String, String> rowContentsList = new LinkedHashMap<String, String>();
            for(int j=0; j<eachRow.size(); j++) {
                JSONObject cell = (JSONObject) eachRow.get(j);
                // log.debug("ADDING COLUMN: name=" + reportHeaders.get(j) + ", label=" + cell.get("label"));
                if(decorators.containsKey(reportHeaders.get(j))) {
                    String columnDecorator = decorators.get(reportHeaders.get(j));
                    rowContentsList.put(columnDecorator, (String)cell.get("label"));
                } else {
                    rowContentsList.put(reportHeaders.get(j), (String)cell.get("label"));
                }
            }
            allRowsList.put(""+i, rowContentsList);
        }
        //printReportData(allRowsList);
        // JSONObject data = reportDataToJson(allRowsList);
        // log.debug("As JSON: " + data.toString());
        log.debug("Exiting cleanse(): Total Rows in Report=" + allRowsList.size());

        return allRowsList; //data.toString();
    }


    private void printReportData(LinkedHashMap<String, LinkedHashMap> allRowsList) {
        StringBuilder data = new StringBuilder();
        for(String key: allRowsList.keySet()) {
            LinkedHashMap<String, String> rowContentsList = allRowsList.get(key);
            data.append("row: [");
            for(String colKey: rowContentsList.keySet()) {
                String colValue = rowContentsList.get(colKey);
                data.append("<" + colKey + "=" + colValue + ">");
            }
            data.append("] ");
        }
        log.debug(data.toString());
    }

    private JSONObject reportDataToJson(LinkedHashMap<String, LinkedHashMap> allRowsList) {
        JSONArray outAllRows = new JSONArray();
        for(String key: allRowsList.keySet()) {
            LinkedHashMap<String, String> rowContentsList = allRowsList.get(key);

            Map<String, String> outColumns = new LinkedHashMap<String, String>();
            for(String colKey: rowContentsList.keySet()) {
                String colValue = rowContentsList.get(colKey);
                outColumns.put(colKey, colValue);
            }
            JSONObject outColumnsAsJson = new JSONObject();
            outColumnsAsJson.put(key, outColumns);
            outAllRows.add(outColumnsAsJson);
        }
        JSONObject data = new JSONObject();
        data.put("datarows", outAllRows);
        return data;
    }

    // Delete ME???
    private JSONObject linkDataToAEMPages(LinkedHashMap<String, LinkedHashMap> allRowsList, String pagePathPrefix) {
        for(String key: allRowsList.keySet()) {
            LinkedHashMap<String, String> rowContentsList = allRowsList.get(key);

            for(String colKey: rowContentsList.keySet()) {
                if(StringUtils.equalsIgnoreCase(colKey, "brandreferencekey")) {
                    String colValue = rowContentsList.get(colKey);
                }
            }
            JSONObject outColumnsAsJson = new JSONObject();
        }
        return null;
    }

    protected ArrayList<String> getHeaderInfo(JSONObject rootObject) {
        log.debug("Inside getHeaderInfo()");
        ArrayList<String> listOfHeaders = new ArrayList<String>();
        JSONArray metaInfo = (JSONArray)((JSONObject) rootObject.get("reportMetadata")).get("detailColumns");
        for(int i=0; i<metaInfo.size(); i++) {
            listOfHeaders.add((String)metaInfo.get(i));
        }

        log.debug("Exiting getHeaderInfo(listofHeaders.size()=" + listOfHeaders.size() + ")");
        return listOfHeaders;
    }
    
    private HashMap<String, String> getColumnDecorator() {
        HashMap<String, String> decorators = new HashMap<String, String>();
        
        decorators.put("Quality_Assurance__c.Property__c.Name", "propertycode");
        decorators.put("QA_Item__c.Brand_Code__c", "brandcode");
        decorators.put("Quality_Assurance__c.QA_Region__c", "region");
        decorators.put("QA_Item__c.Name", "name");
        decorators.put("QA_Item__c.Brand_Reference_Key__c", "brandreferencekey");
        decorators.put("QA_Item__c.Type__c", "type");
        decorators.put("QA_Item__c.Brand_Reference_Numbers__c", "brandreferencenumbers");
        decorators.put("QA_Item__c.Item_Description__c", "description");
        decorators.put("QA_Item__c.Cleanliness__c", "cleanliness");
        decorators.put("QA_Item__c.Compliance__c", "compliance");
        decorators.put("QA_Item__c.Condition__c", "condition");
        
        return decorators;
    }
}
