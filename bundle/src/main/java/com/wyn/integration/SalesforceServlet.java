package com.wyn.integration;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.SlingServletException;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;
import java.util.LinkedHashMap;
import javax.xml.bind.DatatypeConverter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides AJAX/JSON Servlet
 * Example: http://localhost:4502/bin/aemsalesforce?prop=37030&brand=TL
*/
@Service(value={
        SalesforceServlet.class,
        javax.servlet.Servlet.class
 })

@Component(immediate = true, metatype = true)
@Properties({
		@Property(name="sling.servlet.paths", value = "/bin/aemsalesforce"),
        @Property(name="sling.servlet.selectors",value="aemsalesforce"),
        @Property(name="sling.servlet.extensions",value="json")
})

public class SalesforceServlet extends SlingAllMethodsServlet {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());
    
    private final static String ERROR_MSG = "{\"error\":\"true\",\"msg\":\"Missing prop and/or brand\"}";
    private final static String SYS_MSG = "{\"error\":\"true\",\"msg\":\"Unknown Error\"}";

    @Reference
    private SalesforceClient salesforceClient;

    public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws SlingServletException, IOException {
        try {
            response.setContentType("application/json");
            
            PrintWriter out = response.getWriter();
            
            String propertyCode = request.getParameter("prop");
            String brandCode = request.getParameter("brand");
            String responseData = "";

            if(StringUtils.isNotBlank(propertyCode) && StringUtils.isNotBlank(brandCode)) {
                LinkedHashMap dataAsMap = salesforceClient.getSFReportDataForPropertyAndBrandCode(propertyCode, brandCode);
                responseData = (new SimplerSFData()).toJSONString(dataAsMap);
            } else if(StringUtils.isNotBlank(propertyCode)) {
                LinkedHashMap dataAsMap = salesforceClient.getSFReportDataForPropertyCode(request.getParameter(propertyCode));
                responseData = (new SimplerSFData()).toJSONString(dataAsMap);
            } else {
                responseData = ERROR_MSG;
            }
            // if(StringUtils.contains(responseData, "T!T")) {
                // responseData = StringUtils.replace(responseData, "T!T", "TnT");
            // }
            // SimplerSFData.translate(responseData);
            //out.print(responseData);
            // out.print(SimplerSFData.translate(responseData));
            out.print(responseData);
            response.setStatus(HttpServletResponse.SC_OK);
            out.flush();
            out.close();
        } catch(Exception ex) {
            log.error("Exception in doGet()", ex);
            try {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } catch(Exception iex) { }
            throw new SlingServletException(new ServletException("Error servicing your request: " + ex.toString()));
        }
    }
}
